var merge = require('merge');

var status = function(req, res) {
	res.send({
		status : "OK",
		version : "Kraken-Exchange image v0.0.1",
		is_healthy : true
	});
};

function checkDCPrice(book, quantity, type, payment_currency, callback) {
	var total_quantity = 0;
	for (var i = 0; i < book.length; i++) {
		var current_price = book[i][0];
		var current_quantity = book[i][1];
		total_quantity = total_quantity + parseFloat(current_quantity, -1.);
		if (total_quantity > quantity) {
			callback({
				price : current_price,
				currency : payment_currency,
				type : type
			});
			return;
		}
	}
	callback({
		error : "order too large to fill"
	});
};

function checkFiatPrice(book, amount, type, payment_currency, callback) {
	var total_size = 0;
	for (var i = 0; i < book.length; i++) {
		current_price = book[i][0];
		quantity = book[i][1];
		var size = parseFloat(current_price, -1.) * parseFloat(quantity, -1.);
		total_size = total_size + size;
		if (total_size > amount) {
			callback({
				price : current_price,
				currency : payment_currency,
				type : type
			});
			return;
		}
	}
	callback({
		error : "order too large to fill"
	});
}

function orderbookPrice(options, kraken, amount, type, fill_type, callback) {
	kraken.Public.orderbook(options, function(err, data) {
		if (err) {
			console.log(err);
			callback({
				error : err
			});
		} else {
      var book = data['result'][options.pair];
		  var total_size = 0.;
			if (type === 'buy') {
				book = book.asks;
			} else {
				book = book.bids;
			}
			if (fill_type === 'fiat')
				checkFiatPrice(book, amount, type, options.payment_currency, callback);
			else
				checkDCPrice(book, amount, type, options.payment_currency, callback);
		}
  });
}

var price = function(req, res) {
	var currency = req.body.currency;
	var amount = req.body.amount;
	var type = req.body.type;
	var key = req.body.key;
	var secret = req.body.secret;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
    pair : 'XXBTZEUR',
		count : 100
	};
	orderbookPrice(options, kraken, amount, type, 'fiat', function(result) {
		res.send(result);
	});
};

var priceDC = function(req, res) {
	console.log(req.body);
	var currency = req.body.currency;
	var quantity = req.body.quantity;
	var type = req.body.type;
	var key = req.body.key;
	var secret = req.body.secret;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
		pair : 'XXBTZEUR',
		count : 100
	};
	orderbookPrice(options, kraken, quantity, type, 'dc', function(result) {
		res.send(result);
	});
};

var balance = function(req, res) {
	console.log(req.body);
	var currency = req.body.currency;
	var key = req.body.key;
	var secret = req.body.secret;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
		pair: 'XXBTZEUR'
	};
	kraken.Private.balance(options, function(err, data) {
    console.log(data)
		if (err) {
			console.log(err);
			res.send({
				error : err
			});
		}
		else {
      var balance;
      // if currency is not BTC then assume EUR
      if (currency === 'BTC') {
        balance = data.result.XXBT;
      } else {
        balance = data.result.ZEUR;
      }

      if (balance === undefined) {
        balance = 0;
      }

			res.send({
				balance : balance,
				currency : currency
			});
		
		}
	});
};

var withdraw = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var currency = req.body.currency;
	var quantity = req.body.quantity;
	var address = req.body.address;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
		currency : currency,
		quantity : quantity,
		address : address
	};
	kraken.Private.transfer(options, function(err, r, data) {
		if (err) {
			res.send({
				error : err
			});
		}
		else if (data.status === 'success') {
			console.log(data);
			res.send({
				txid : data.data.txid
			});
		} else {
			console.log('KRAKEN ERROR');
			console.log(data);
			res.send({
				error : data.message
			});
		}
	});
};

var placeOrder = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var kraken = require('./kraken-api')(key, secret);
	var buffer = parseFloat(req.body.buffer);
	var type = req.body.type;
	var digital_currency = req.body.digital_currency;
	var fiat_currency = req.body.fiat_currency;
	var wfid = req.body.wfid;
	var quantity;
	if (type === 'buy') {// AMOUNT DC TO BUY
		quantity = req.body.amount_dc;
		fill_type = 'dc';
	} else if (type === 'sell') {//AMOUNT FIAT to SELL
		quantity = req.body.amount_fiat;
		fill_type = 'fiat';
	} else {
		//fuck you vignesh
	}
	var options = {
    pair : 'XXBTZUSD',
		count : 100
	};

	orderbookPrice(options, kraken, quantity, type, fill_type, function(result) {
		if (result.error) {
			res.send({
				error : result.error,
				wfid : wfid
			});
		} else {
			console.log('Initial price');
			price = result.price;
			console.log(price);
			var trade_options;
			if (type === 'buy') {
				price = price * (1 + buffer );
				console.log('Buffered price');
				console.log(price);
				var amount = quantity;
				//Satoshis
				bit_obj = toCurrencyObjDigital(8, amount);
				price_obj = toCurrencyObjFiat(2, price);
				trade_options = {
          pair : 'XXBTZEUR',
					type : 'buy',
          ordertype : 'market',
          volume : bit_obj.value,
					price : price_obj.value
				};
				console.log(trade_options);
			} else if (type === 'sell') {
				price = price * (1 - buffer );
				console.log('Buffered price');
				console.log(price);
				var amount = quantity / price;
				//Dollars
				bit_obj = toCurrencyObjDigital(8, amount);
				price_obj = toCurrencyObjFiat(2, price);
				trade_options = {
          pair : 'XXBTZEUR',
					type : 'sell',
          ordertype : 'market',
					volume : bit_obj.value,
					price : price_obj.value
				};
				console.log(trade_options);
			} else {
				//impossible
			}
			console.log('trading');
			kraken.Private.place(trade_options, function(err, data) {
				if (err) {
					res.send({
						error : err,
						wfid : wfid
					});
				} else {
					console.log(data.result);
					res.send({
						order_id : data.result.txid,
						wfid : wfid,
						price_placed : trade_options.price
					});
				}
      });

		}
	});
};

var getOrders = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var number = req.body.number;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
		trades: true
	};
	kraken.Private.closedOrders(options, function(err, data) {
		if (err) {
			res.send({
				error : err
			});
		} else {
      console.log(data);
			var pending = [];
			var closed = [];
			var open = [];
			var canceled = [];
			var expired = [];
			var unknown = [];
      var orders = data.result.closed;
			for (var order_id in orders) {
        var current_order = null;
        if (orders.hasOwnProperty(order_id)) {
				  var current_order = orders[order_id];
        }
        if (!current_order) {
          continue;
        }

				if (current_order.status === 'open') {
					open.push({
						order_id : order_id
					});
				} else if (current_order.status === 'closed') {
					closed.push({
						order_id : order_id
					});
				} else if (current_order.status === 'pending') {
					pending.push({
						order_id : order_id
					});
				} else if (current_order.status === 'canceled') {
					canceled.push({
						order_id : order_id
					});
				} else if (current_order.status === 'expired') {
					expired.push({
						order_id : order_id
					});
				} else {
					unknown.push({
						order_id : order_id
					});
				}
			}
			var l = {
				closed : closed,
				open : open,
				canceled : canceled,
				expired : expired,
				expired : expired,
				unknown : unknown
			};
      console.log(l);
			res.send(l);
		}
	});
};

var getOrderDetails = function(req, res) {
	var key = req.body.key;
	var secret = req.body.secret;
	var order_id = req.body.order_id;
	var kraken = require('./kraken-api')(key, secret);
	var options = {
		txid: order_id
	};
	console.log(order_id);

	kraken.Private.orderDetails(options, function(err, data) {
		if (err) {
			res.send({
				error : err
			});
		} else {
			orders = data.result;
			var l;
			if (orders.length === 0) {
				console.log('empty order');
				l = {
					order_id : order_id,
					status : 'open'
				};
			} else {
				var type;
				var status;
				var order;
				total_digital_currency = 0;
				total_fiat_currency = 0.;
				total_fee= 0.;
				
        Object.keys(orders).forEach(function(order_id) {
          order = orders[order_id];
					type = order.descr.type;
					status = order.status;
					total_digital_currency += parseFloat(order.vol,0);
					total_fiat_currency += parseFloat(order.cost,0);
					total_fee += parseFloat(order.fee,0);
				});
				l = {
					order_id : order_id,
					average_price : order.price.value,
					type : type,
					status : status,
					total_digital_currency : total_digital_currency.toString(),
					total_fiat_currency: total_fiat_currency,
					fee: { 
						fiat_currency:total_fee
					}
				};
			}

			res.send(l);
    }
	});
};

var toCurrencyObjFiat = function(precision, value) {
	try{
		value = parseFloat(value);
	}catch(err){
		value = 0.;
	}
	value_int = value * (Math.pow(10, precision));
	value_int = Math.floor(value_int);
	return {
		precision : precision,
		value : value.toString(),
		value_int : value_int
	};
};

var toCurrencyObjDigital = function(precision, value) {
	try{
		value = parseFloat(value);
	}catch(err){
		value = 0.;
	}
	value_int = value.toFixed(precision) * (Math.pow(10, precision));
	value_int = Math.floor(value_int);
	return {
		precision : precision,
		value : value.toFixed(precision),
		value_int : value_int
	};
};

exports.status = status;
exports.price = price;
exports.priceDC = priceDC;
exports.balance = balance;
exports.placeOrder = placeOrder;
exports.getOrders = getOrders;
exports.getOrderDetails = getOrderDetails;
exports.withdraw = withdraw;
