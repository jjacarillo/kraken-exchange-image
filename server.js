var express = require('express');

// this is the service provider
var api = require('./api.js');

// the REST server instance
var server = express();
server.set('VOS-EXCHANGE', 'NodeJS REST Server');

server.configure(function(){
  server.use(express.bodyParser());
});

// simple logger
server.use(function(req, res, next){
    console.log('%s %s', req.method, req.url); //For security, we do not log.
    res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// API routes
server.get('/', api.status);
server.get('/v1/health', api.status);
server.post('/v1/price', api.price);
server.post('/v1/priceDC', api.priceDC);
server.post('/v1/balance', api.balance);
server.post('/v1/placeOrder', api.placeOrder);
server.post('/v1/getOrders', api.getOrders);
server.post('/v1/getOrderDetails', api.getOrderDetails);
server.post('/v1/withdraw', api.withdraw);

server.listen(process.env.PORT || 9050, function() {
  console.log('"%s" listening at localhost:%s', server.name, process.env.PORT || 9050);
});
