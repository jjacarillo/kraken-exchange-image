var _ = require('underscore'),
    request = require('request');

var API = function(api_key, api_secret) {

    var settings = {
        api_key: api_key,
        api_secret: api_secret
    };


    return {
        Public: require('./routes/public')(settings),
        Private: require('./routes/private')(settings)
    };
}

module.exports = API;
