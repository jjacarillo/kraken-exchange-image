var _ = require('underscore'),
    Core = require('../core/index'),
    Validator = require('../core/validator'),
    request = require('request'),
    crypto = require('crypto'),
    qs = require('querystring'),
		KrakenClient = require('kraken-api');

function Call(endpoint, options, cb) {
    if(arguments.length === 2) {
         // there are no args!
         var cb = options;
         options = {};
    }

    var params = {};

    // check required params
    if(!_.isUndefined(options.requires)) {
        Validator.check(options.requires, options.params);
        _.each(options.requires, function(v, i) {
						if (v)
							params[v] = options.params[v]; 
        });
    }

    // check optional params
    if(!_.isUndefined(options.optional)) {
        _.each(options.optional, function(v, i) {
            if(!_.isUndefined(options.params[v])) {
                params[v] = options.params[v];
            }
        });
    }

		// kraken client
		console.log('Connecting to Kraken API...');
		var kraken = new KrakenClient(Private.settings.api_key, Private.settings.api_secret);
		kraken.api(endpoint, params, cb);
    console.log(endpoint);
    console.log(params);
    //Core.get(endpoint, params, cb, Public.settings);
}

var Private = {
    balance: function(opts, cb) {
        Call('Balance', {
            requires: ''.split(' '),
            params: opts
        }, cb);
    },
    place: function(opts, cb) {
        Call('AddOrder', {
            requires: 'pair type ordertype price volume'.split(' '),
            params: opts
        }, cb);
    },
    openOrders: function(opts, cb) {
        Call('OpenOrders', {
            requires: 'trades'.split(' '),
            params: opts
        }, cb);
    },
    closedOrders: function(opts, cb) {
        Call('ClosedOrders', {
            requires: 'trades'.split(' '),
            params: opts
        }, cb);
    },
    trades: function(opts, cb) {
        Call('TradesHistory', {
            requires: ''.split(' '),
            params: opts
        }, cb);
    },
    orderDetails: function(opts, cb) {
        Call('QueryOrders', {
            requires: 'txid'.split(' '),
            params: opts
        }, cb);
    }
};

module.exports = function(settings) {
		Private.settings = settings;
    return Private;
}
