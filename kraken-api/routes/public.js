var _ = require('underscore'),
    Core = require('../core/index'),
    Validator = require('../core/validator'),
    request = require('request'),
    crypto = require('crypto'),
    qs = require('querystring'),
		KrakenClient = require('kraken-api');

function Call(endpoint, options, cb) {
    if(arguments.length === 2) {
         // there are no args!
         var cb = options;
         options = {};
    }

    var params = {};

    // check required params
    if(!_.isUndefined(options.requires)) {
        Validator.check(options.requires, options.params);
        _.each(options.requires, function(v, i) {
            if(!_.isUndefined(options.params[v])) {
                params[v] = options.params[v];
            }
        });
    }

    // check optional params
    if(!_.isUndefined(options.optional)) {
        _.each(options.optional, function(v, i) {
            if(!_.isUndefined(options.params[v])) {
                params[v] = options.params[v];
            }
        });
    }

		// kraken client
		console.log('Connecting to Kraken API...');
		var kraken = new KrakenClient(Public.settings.api_key, Public.settings.api_secret);
    //console.log(params);
		kraken.api(endpoint, params, cb);
    //Core.get(endpoint, params, cb, Public.settings);
}

var Public = {
    ticker: function(opts, cb) {
        Call('Ticker', {
            requires: 'pair'.split(' '),
            params: opts
        }, cb);
    },
    orderbook: function(opts, cb) {
        Call('Depth', {
            requires: 'pair'.split(' '),
            optional: 'count'.split(' '),
            params: opts
        }, cb);
    }
};

module.exports = function(settings) {
		Public.settings = settings;
    return Public;
}
